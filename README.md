# Gamification of Energy Bills

## Timeline

There are several key deadlines for this project, shown here:

![timeline](images/Timeline.png)

## Overview

The idea is to help customers to engage by “gamifying” their energy bill, allowing them to compare their spend with others in their area and giving them context about spending e.g. you spend x more than people on your street / y more than other families of 4. We should also provide incentives (in the form of rewards / stars / ratings, which could in future be monetary) to shift their demand away from peak. We should help them by giving them tips on what they could do e.g. if you ran your washing machine overnight you would save £x. 

This application should link in with their smart meter data (this is likely to be half hourly consumption data) as well as (time of use) tariff data which represents when generation / distribution peaks are in order to help customers use energy at the cheapest time and potentially via “green” resources.

Additional developments could include information on the rate of return for “green products” e.g. buying a heat pump or an electric vehicle.

## Constraints

Should be developed as a native app (iOS and / or Android) or as a web app that is supported by the majority of mobile browsers (Safari, Chrome and Samsung Internet collectively make up ~95% of the UK mobile browser market share ).

This should be developed as an app, data from the smart meter will be received via xx. This data can be provided as a separate file as an interim stage to develop the product. Integration to more “live” data sources can be a secondary step. We also need to be mindful that customers’ smart meter data should be kept private.

## Research

There are many existing apps for monitoring the usage of energy within a home, here a some that we have researched:

- [EnergyCloud](https://www.bluelineinnovations.com/features): 
  - Provides real time feedback
  - The ability to view trends of usage giving feedback on whether cost saving measures are working
  - Comparing results of different months
  - Analyzing habits
  - Access information from anywhere
- [Nest app](https://nest.com/uk/app/):
  - Access information from anywhere
  - Insights into usage
  - Share information with others
- [Neurio](https://www.neur.io/):
  - Similar features to EnergyCloud
  - Works with solar panels and renewables
- [Wattsly](https://energyappsontario.devpost.com/submissions/20053-wattsly):
  - Graphs of usage that have changeable units
    - kWh, cost, range of an electric car, etc.
  - Ability to tag certain times with what appliances you were using
- [TheGenGame](https://www.thegengame.com/):
  - Notificications at peak times to try and reduce
  - Earn points based on how much you can save
  - Leagues for competing users

The best examples for inspiration are "Wattsly" and "TheGenGame". These give good examples of processing information and making it easy to understand and engage with. The notification system that TheGenGame employs is a great idea, and being able to prompt different sections of the a userbase to reduce usage would help even out the load on the grid - This would require some fairly complex algorithms however.

The other common feature that can be seen across examples is the ability to see your usage wherever you are. This is a desirable feature as it allows users to monitor their usage at all times, spotting if they have something wasting energy, seeing times it would have been good to use more energy, etc. 

## OO Design & UML

### Domain Model

![Domain model](images/domain_model.png)

### User flow

![](images/authenticated_flow.png)

# How to install

There are two repositories, one for the backend and frontend, found at the following links:

- https://gitlab.com/gamification-of-bills/gob-backend
- https://gitlab.com/gamification-of-bills/gob-frontend

