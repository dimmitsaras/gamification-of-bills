# Requirements

## Stakeholders

### Baringa partners

An independent business and technology consultancy (specifically for OVO and Bristol energy), want to help businesses run for effectively.

### OVO energy

Want to reduce energy required during peak usage times as it is more expensive

### Bristol energy

Similar motivation to OVO

### People with smart meters

Would like to spend less on energy and potentially be more aware of usage

### Power generation companies

Want to produce less energy at peak hours

### The national grid

Want to reduce stress on power network at peak hours

### Entities affected by climate change

Want less energy usage to help combat climate change

## Use-case diagram

TODO