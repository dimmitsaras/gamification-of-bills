# OO Design & UML

- [x] Provide a high-level architecture diagram of your application. 
- [x] The diagram should make clear if you are building a client-server application, or a stand alone application. 
- [x] It should include relevant external systems (if applicable) your application depends on.

- [x] For the diagram, add two or three paragraphs that explain what the meaning of the components represented in the diagram.

- [ ] Provide one example of a static and a dynamic UML modelling aspect of your system.

Together with the diagram provide a section of written text that describes:

- [ ] The context within which you created this diagram. This might for example be the use case that you were working on or modelling.
- [ ] The motivation behind your choice of this particular aspect. This might be due to a challenging design decision or uncertainty about the relationship of domain concepts.
- [ ] A brief reflection on the modelling choices you made and any knowledge that you gained from this model.

## Domain model

To aid the design of our system, a domain model has been created to show the existing systems and how they interact:

![](../images/domain_model.png)

## Architecture

### Overview

![](../images/architecture.png)

The application will consist of several parts, An app for the end user, a database for storing usage information and an API for communicating with the database and the app. Throughout development data will be provided as a CSV with the following format:

__id__ (unique account number, `integer`), __start_time__ (UTC, format _2017-09-03 13:00:00 UTC_ ), __consumption__ (KWh, `float`: _0.072_)

This will be added to the database via simple script.

The usage database will be a MySQL db (using MariaDB) with the following layout:

```mssql
energy_usage (id INTEGER PRIMARY KEY, start_time TIMESTAMP, consumption FLOAT)
```

The database will be only accessible by the API, therefore by ensuring sanitised inputs from the app we can keep all data secure.

The API will be RESTful and implemented using Spring (Java). Whilst the functionality of the API will be minimal at first, it can be expanded to provide more powerful functionality. By processing data on a server, we can both ensure data security and allow more expensive computations.

The app will be created using Flutter (Dart). This framework allows us to create native cross-platform applications in an object orientated way. An example of the flutter class structure is shown below:

![](../images/flutter_flow.png)

## Dynamic UML

### Authenticated flow

Below is a diagram of an example usage of that app and how data is transferred between various parts of the architecture

![](../images/authenticated_flow.png)