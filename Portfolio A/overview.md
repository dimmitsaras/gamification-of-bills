# Overview

The goal of this project is to create an app that will help increase user engagement with their energy bills, by creating an app that "gamifies" the information and allows them to easily see their energy usage, incentivising them to reduce this. The app can contain many features such as comparing past usage (e.g. current month/week compared to previous), allowing them to compare their spending with others in their area, or providing a reward points system for good usage, however we will first focus on a subset of these features and improve upon this if possible.

## MVP

The minimum viable product will have the following features 

- A simple app that will allow a user to see their current usage and compare it to previous usage (e.g. the last month, week etc.).
- The data will be provided by OVO and will be half hourly consumption data recieved from homes with smart meters.
- A user will have an ID (linking their energy bills and usage data) which will allow them to login and see their data

## Final product (subject to change)

- Notifications when peak usage is to help users reduce
- Rate of return for "green products" e.g. buying electric vehicles
- Points based system based on usage at different times
- Opt-in "Leagues" to allow users to compete for the best usage
- Tips for reducing usage

## Long term features

- Integration of "live" data sources